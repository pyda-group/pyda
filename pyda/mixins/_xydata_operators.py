#  Copyright 2022 Martin Hewitson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import numpy
import numbers
import pyda
from pyda.utils._pyda_obj import _pyda_obj


class XYDataOperators:

    # ----------------------------------------------------
    # Operators
    # ----------------------------------------------------
    def __truediv__(self, other):

        t1 = self.deepcopy()
        t2 = other

        # handle special case of "other" being a number
        if isinstance(t2, numbers.Number):
            t1.yaxis.data = t1.yaxis.data / t2
            t1._name = t1._name + "/" + str(t2)
            return t1

        # check length
        if t2.yaxis.data.size > 1 and t1.yaxis.data.size != t2.yaxis.data.size:
            raise Exception(
                "Can only divide objects of same length: "
                + str(t1.size)
                + " != "
                + str(t2.size())
            )

        # error propagation
        y1 = t1.yaxis.data.squeeze()
        y2 = t2.yaxis.data.squeeze()
        dy1 = t1.yaxis.ddata.squeeze()
        dy2 = t2.yaxis.ddata.squeeze()
        dy = numpy.sqrt(
            (dy1 / y2) ** 2
            + ((-y1 / y2**2) * dy2) ** 2
            + ((y1 / y2**3) * (dy2**2)) ** 2
            + (((-1.0 / y2) ** 2) * dy1 * dy2) ** 2
        )
        t1.yaxis.ddata = dy

        # divide data
        t1.yaxis.data = y1 / y2
        t1.yaxis.units /= t2.yaxis.units

        # name
        t1.name = t1.name + "/" + t2.name

        return t1

    def __pow__(self, power, modulo=None):
        t1 = self.deepcopy()

        # print("xydata/pow")
        # print(t1)
        # print(power)

        if isinstance(power, pyda.ydata.YData):
            if power.yaxis.data.size != 1:
                raise Exception("Need single valued YData to raise to power")
            power = power.ydata()[0]

        # error propagation
        y1 = t1.yaxis.data
        dy = t1.yaxis.ddata
        dy = dy * numpy.abs(power * y1 ** (power - 1))

        # raise to power
        t1.yaxis.data **= power
        t1.yaxis.units **= power
        t1.yaxis.ddata = dy

        t1.name = t1.name + "**" + str(power)

        return t1

    def _rpow__(self, power, modulo=None):
        t1 = self.deepcopy()

        # print("xydata/rpow")
        # print(t1)
        # print(power)

        if isinstance(power, pyda.ydata.YData):
            if power.yaxis.data.size != 1:
                raise Exception("Need single valued YData to raise to power")
            power = power.ydata()[0]

        # error propagation
        y1 = t1.yaxis.data
        dy = t1.yaxis.ddata
        dy = dy * numpy.abs(power * y1 ** (power - 1))

        # raise to power
        t1.yaxis.data **= power
        t1.yaxis.units **= power
        t1.yaxis.ddata = dy

        t1.name = t1.name + "**" + str(power)

        return t1

    def __rsub__(self, other):
        """
        Subtract two data series or a number

        t3 = t2 - t1
        t3 = t2 - 2

        :param other:
        :return:
        """
        t1 = self.deepcopy()
        t2 = other
        # print(t1)
        # print(t2)

        # handle special case of "other" being a number
        if isinstance(t2, numbers.Number):
            t1.yaxis.data = t2 - t1.yaxis.data
            t1._name = str(t2) + "-" + t1._name
            return t1

        if not isinstance(t2, pyda.xydata.XYData) and not isinstance(
            t2, pyda.ydata.YData
        ):
            raise Exception("Second object should be a number or a TSData object")

        # check units
        u1 = t1.yunits()
        u2 = t2.yunits()
        if u1 != u2:
            raise Exception(
                "Units of object 1 " + u1.char() + " not equal to object 2 " + u2.char()
            )

        # check length
        if t2.yaxis.data.size > 1 and t1.yaxis.data.size != t2.yaxis.data.size:
            raise Exception(
                "Can only subtract objects of same length: "
                + str(t1.size)
                + " != "
                + str(t2.size())
            )

        # error propagation
        y1 = t1.yaxis.data.squeeze()
        y2 = t2.yaxis.data.squeeze()
        dy1 = t1.yaxis.ddata.squeeze()
        dy2 = t2.yaxis.ddata.squeeze()
        dy = numpy.sqrt(dy1**2 + dy2**2)
        t1.yaxis.ddata = dy

        # subtract data
        t1.yaxis.data = y1 - y2

        # handle name
        t1.name = "(" + t1.name + " - " + t2.name + ")"

        return t1

    def __sub__(self, other):
        """
        Subtract two data series or a number

        t3 = t2 - t1
        t3 = t2 - 2

        :param other:
        :return:
        """
        t1 = self.deepcopy()
        t2 = other

        # handle special case of "other" being a number
        if isinstance(t2, numbers.Number):
            t1.yaxis.data = t1.yaxis.data - t2
            t1._name = t1._name + "-" + str(t2)
            return t1

        if not isinstance(t2, pyda.xydata.XYData) and not isinstance(
            t2, pyda.ydata.YData
        ):
            raise Exception("Second object should be a number or a TSData object")

        # check units
        u1 = t1.yunits()
        u2 = t2.yunits()
        if u1 != u2:
            raise Exception(
                "Units of object 1 " + u1.char() + " not equal to object 2 " + u2.char()
            )

        # check length
        if t2.yaxis.data.size > 1 and t1.yaxis.data.size != t2.yaxis.data.size:
            raise Exception(
                "Can only subtract objects of same length: "
                + str(t1.size)
                + " != "
                + str(t2.size())
            )

        # error propagation
        y1 = t1.yaxis.data.squeeze()
        y2 = t2.yaxis.data.squeeze()
        dy1 = t1.yaxis.ddata.squeeze()
        dy2 = t2.yaxis.ddata.squeeze()
        dy = numpy.sqrt(dy1**2 + dy2**2)
        t1.yaxis.ddata = dy

        # subtract data
        t1.yaxis.data = y1 - y2

        # handle name
        t1.name = "(" + t1.name + " - " + t2.name + ")"

        return t1

    def __radd__(self, other):
        out = self.__add__(other)
        return out

    def __add__(self, other):
        """

        :param other:
        :return:
        """
        t1 = self.deepcopy()
        t2 = other

        # handle special case of "other" being a number
        if isinstance(t2, numbers.Number):
            t1.yaxis.data = t1.yaxis.data + t2
            t1._name = t1._name + "+" + str(t2)
            return t1

        if not isinstance(t2, pyda.xydata.XYData) and not isinstance(
            t2, pyda.ydata.YData
        ):
            raise Exception("Second object should be a number or a TSData object")

        # check units
        u1 = t1.yunits()
        u2 = t2.yunits()
        if u1 != u2 and not u1.isEmpty() and not u2.isEmpty():
            raise Exception(
                "Units of object 1 " + u1.char() + " not equal to object 2 " + u2.char()
            )

        # check length
        if t2.yaxis.data.size > 1 and t1.yaxis.data.size != t2.yaxis.data.size:
            raise Exception(
                "Can only add objects of same length: "
                + str(t1.size())
                + " != "
                + str(t2.size())
            )

        # error propagation
        y1 = t1.yaxis.data.squeeze()
        y2 = t2.yaxis.data.squeeze()
        dy1 = t1.yaxis.ddata.squeeze()
        dy2 = t2.yaxis.ddata.squeeze()
        dy = numpy.sqrt(dy1**2 + dy2**2)
        t1.yaxis.ddata = dy

        # add data
        t1.yaxis.data = y1 + y2

        # handle name
        t1.name = "(" + t1.name + " + " + t2.name + ")"

        return t1

    def __rmul__(self, other):
        new_name = self.name
        out = self.__mul__(other)
        # handle name
        if isinstance(other, _pyda_obj):
            oname = other.name
        else:
            oname = str(other)
        out.name = "(" + str(oname) + "*" + new_name + ")"
        return out

    def __mul__(self, other):

        t1 = self.deepcopy()
        t2 = other

        # handle special case of "other" being a number
        if isinstance(t2, numbers.Number):
            t1.yaxis.data = t1.yaxis.data * t2
            t1._name = t1._name + "*" + str(t2)
            return t1

        # handle special case of "other" being a numpy array
        if isinstance(t2, numpy.ndarray):
            t1.yaxis.data = t1.yaxis.data * t2
            t1._name = "(" + t1._name + "*ndarray" + ")"
            return t1

        # check length
        if (
            t2.yaxis.data.size > 1
            and t1.yaxis.data.size > 1
            and t1.yaxis.data.size != t2.yaxis.data.size
        ):
            raise Exception(
                "Can only multiply objects of same length: "
                + str(t1.size)
                + " != "
                + str(t2.size())
            )

        # error propagation
        y1 = t1.yaxis.data.squeeze()
        y2 = t2.yaxis.data.squeeze()
        dy1 = t1.yaxis.ddata.squeeze()
        dy2 = t2.yaxis.ddata.squeeze()
        dy = numpy.sqrt((y2 * dy1) ** 2 + (y1 * dy2) ** 2 + (dy1 * dy2) ** 2)

        # multiply data
        t1.yaxis.data = y1 * y2
        t1.yaxis.units *= t2.yaxis.units

        # set error data
        t1.yaxis.ddata = dy

        # handle name
        t1.name = "(" + t1.name + "*" + t2.name + ")"

        return t1
