#
#
# Martin Hewitson, 2021
# Copyright 2021, by the Max Planck Institute for Gravitational Physics
# (Albert Einstein Institute). ALL RIGHTS RESERVED.
#
from utils import _pyda_obj


def sqrt(val: _pyda_obj = None):
    return val.sqrt()
