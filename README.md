# pyda

Pyda is a python package for signal processing and building models of LTI systems with a simple but powerful interface. 
Currently pyda is in its early development phase.

Pyda adopts a "it just works" principle, where the most common aspects of signal and instrument data analysis
work easily with few lines of code. That said, accessing the underlying data structures also allows more advanced
analysis to be performed.

## Signal processing

Pyda provides classes for encapsulating time and frequency series data. Units and errors are properly handled. Powerful
plotting interface together with signal and spectral processing tools make for an object-oriented environment.

## Control systems

The scope of control system capabilities is currently being defined.

## Usage examples

```python
from pyda.tsdata import TSData
from pyda.dsp.spectral import psd

# create a time-series of random noise (unit PSD)
ts = TSData.randn(nsecs=10000, fs=10, name='ts1', yunits='m')
print(ts)

# create a time-series of random noise (unit PSD)
ts2 = TSData.randn(nsecs=10000, fs=10, name='ts1', yunits='m')/2
print(ts)

# element-wise multiplication
ts3 = ts*ts2
ts3.name = 'ts3'

# Plot
ts.plot(ts2, ts3)

```

See also [Jupyter notebook examples](pyda/Examples).

# Development roadmap

We are currently working on [specifications](https://gitlab.com/pyda-group/pyda/-/wikis/home/specifications) for Pyda. See also [meetings](https://gitlab.com/pyda-group/pyda/-/wikis/home/meetings) and 
[Issues](https://gitlab.com/pyda-group/pyda/-/issues) for more information. 

The project is open for contributions, and feedback on usage is welcome. For reporting bugs and specific code suggestions
please consider [creating an Issue](https://gitlab.com/pyda-group/pyda/-/issues/new?issue%5Bmilestone_id%5D=). 
For other enquiries contact details are provided below.

# Contact

* Martin Hewitson (martin.hewitsonATaeiDOTmpg.de)
* Artem Basalaev (artem.basalaevATphysikDOTuni-hamburg.de)
* Christian Darsow-Fromm (cdarsowfATphysnetDOTuni-hamburg.de)
* Oliver Gerberding (oliver.gerberdingATphysikDOTuni-hamburg.de)


# License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.


