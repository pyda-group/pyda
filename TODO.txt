
add XYZData
- spectrogram method

TO INSTALL SINCE 2022-12
pip install .


update lpsd



poetry run pre-commit install
poetry install


 python setup.py install --force

Branch/Commit/Merge-request process

                gcc -arch x86_64 -c -fPIC ltpda_dft.c && \
                gcc -arch x86_64 -shared -o ltpda_dft.so ltpda_dft.o -Wl



1. Moved the LISA code out of pyda on my local repository
2. (Here’s the mistake) committed the removal to my local master branch
3. Now I can’t push master, as we wanted
4. Created an issue and merge request on gitlatb
5. Checked out the new branch
6. Merged local master into that new branch
7. Closed the git issue (which merges into master?)
8. Pulled master from gitlab
9. I’m still left with stuff on my local master that I should push, but can’t :(

Sure, I think actually it's not so bad. It did not go as expected because you closed your merge request instead of merging it, otherwise it looks fine. In git jargon "close" merge request means discard it, stop working on it. I just reopened it again. To merge you use "merge" button which is not available because you put "Draft:" in the name. Just rename it and the button should become available.



make helper to apply methods to an axis
- pass in axis and error function
- implement more xydata operators (sin, cos, tan, etc)

Architecture:
- currently we have plot() etc as methods of XYData etc
    - might be better to have Plotter class, and TSPlotter, FSPlotter
        import TSPlotter as tsplt
        tsplt.plot(ts1, ts2, ts3)
        tsplt.loglog(ts1)
- similar for spectral methods - these in principle operate on TSData objects, but are in their own class
    - what's a good design pattern to extend a class here?
    - or is it better to have this type of module that applies methods? But then we have to assume things about the
    input objects...

NEW: started using Mixin classes. So far for plotting and diff on XYData class
-- Should filter and spectral stuff be mixins? In principle they only work on TSData, so makes sense.
-- make YData mixins

Should we vectorize psd() etc? Use *args?
- in MATLAB, an array of objects has the class of the objects, but not in python - there we need a list or so

Move some setter checking stuff into Axis class

For Simscape class, maybe better store data set keyed by object name, rather than as a list?

Better information export from Simscape to keep with dataset?
    - export all mat config files because they are hard to read in python


Add help texts!

Finish fpsder derivative

Add
- ltfe
- lcohere
- etc


- utility filter methods on tsdata class

- tsdata noise generator
    - need pole/zero interface, but maybe don't need a pzmodel class yet



** lpsd compilation for Mac M1

check for long doubles everywhere - they are slow on Mac M1
the polyreg part seems to take half the run-time
write compilation instructions
